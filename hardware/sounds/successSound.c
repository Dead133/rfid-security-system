#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <wiringPi.h>
#include <softTone.h>

#define	PIN	6

int main () {
  wiringPiSetup () ;

  softToneCreate (PIN) ;

  softToneWrite(PIN, 600);
  delay(100);
  softToneWrite(PIN, 0);
  delay(100);
  softToneWrite(PIN, 600);
  delay(100);
  softToneWrite(PIN, 0);
}
