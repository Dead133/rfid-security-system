#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <wiringPi.h>
#include <softTone.h>

#define	PIN	6

int main () {
  wiringPiSetup () ;

  softToneCreate (PIN) ;

  softToneWrite(PIN, 100);
  delay(400);
  softToneWrite(PIN, 0);
}
