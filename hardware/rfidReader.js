'use strict';

var EventEmitter = require('events').EventEmitter;
var util = require('util');
var SerialPort = require('serialport').SerialPort;

var PORT = '/dev/ttyAMA0';

// RDM6300 Flags
var START_FLAG = '\x02';
var END_FLAG = '\x03';

/**
 * RDM6300 data parser
 *
 * @returns {function}
 */
function parser() {
    // Delimiter buffer saved in closure
    var data = '';
    return function (emitter, buffer) {
        // Collect data

        var dataCharacter = buffer.toString('utf8');

        switch (dataCharacter) {
            case START_FLAG:
                //console.log('Start flag received!');
                data = '';
                break;
            case END_FLAG:
                //console.log('End flag received!');
                emitter.emit('data', data);
                break;
            default:
                data += dataCharacter;
        }
    };
}

var SERIALPORT_SETTINGS = {
    baudrate: 9600,
    dataBits : 7,
    parity : 'odd',
    stopBits: 1,
    bufferSize: 1,
    flowControl : false,
    parser: parser()
};

/**
 * Class RfidReader
 *
 * @constructor
 */
var RfidReader = function() {
    var self = this;
    var serialPort = new SerialPort(PORT, SERIALPORT_SETTINGS, false);

    serialPort.open(function (error) {
        if (error) {
            self.emit('error', 'Initialization error. Failed to open port: ' + error);
            return;
        }

        serialPort.on('data', function(data) {
            self.emit('data', data);
        });
    });
};

util.inherits(RfidReader, EventEmitter);

module.exports = new RfidReader();
