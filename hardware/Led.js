'use strict';

var gpio = require('rpi-gpio');

var GPIO_PINS = [
    7,
    11,
    12,
    13,
    15,
    16,
    18,
    22
];

function checkPin(pin) {
    if (GPIO_PINS.indexOf(pin) === -1) {
        throw new Error('Invalid GPIO pin: ' + pin);
    }
}

/**
 * Class Led
 *
 * @param {int} pin
 *
 * @constructor
 */
var Led = function (pin) {
    checkPin(pin);

    var _pin = pin;
    var _isPortOpen = false;
    var _timeout = null;

    var self = this;

    gpio.setup(pin, gpio.DIR_OUT, function (error) {
        if (error) {
            throw new Error('Cannot open GPIO port ' + pin + ', error: ' + error);
        }

        _isPortOpen = true;

        self.turnOff();
    });

    /**
     * Turn the LED on
     */
    self.turnOn = function () {
        var self = this;

        if (_timeout !== null) {
            clearTimeout(_timeout);
            _timeout = null;
        }

        if (!_isPortOpen) {
            setTimeout(function () {
                self.turnOn();
            }, 100);
            return;
        }

        gpio.write(_pin, true, function (error) {
            if (error) {
                console.log(error);
            }
        });
    };

    /**
     * Turn the LED off
     */
    self.turnOff = function () {
        var self = this;

        if (_timeout !== null) {
            clearTimeout(_timeout);
            _timeout = null;
        }

        if (!_isPortOpen) {
            setTimeout(function () {
                self.turnOff();
            }, 100);
            return;
        }

        gpio.write(_pin, false, function (error) {
            if (error) {
                console.log(error);
            }
        });
    };
};

module.exports = Led;
