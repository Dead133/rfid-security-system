'use strict';

var EventEmitter = require('events').EventEmitter;
var util = require('util');
var LcdDisplay = require('lcd');
var DISPLAY_ROWS = 2;
var DISPLAY_COLS = 16;
var DISPLAY_CONFIG = {
    rs: 4,
    e: 17,
    data: [
        27,
        10,
        9,
        11
    ],
    cols: DISPLAY_COLS,
    rows: DISPLAY_ROWS
};

/**
 * Trims string to match LCD line character limit
 *
 * @param {string} string
 *
 * @returns {string}
 */
function trimString(string) {
    return string.substring(0, DISPLAY_COLS);
}

/**
 * Adds spaces to string to match LCD line character limit
 *
 * @param {string} string
 *
 * @returns {string}
 */
function addSpacesToString(string) {
    while (string.length < DISPLAY_COLS) {
        string += ' ';
    }
    return string;
}

/**
 * Class Lcd
 *
 * @constructor
 */
var Lcd = function () {
    var self = this;

    this._display = new LcdDisplay(DISPLAY_CONFIG);
    this._isReady = false;

    this._display.on('ready', function () {
        self._isReady = true;
        self.emit('ready');
    });

    /**
     * Returns true if LCD is ready for use
     *
     * @returns {boolean}
     */
    this.isReady = function () {
        return this._isReady;
    };

    /**
     * Set the first line of text on the LCD
     *
     * @param {string} text
     */
    this.firstLine = function (text) {
        if (!this._isReady) {
            return;
        }

        text = trimString(text);
        text = addSpacesToString(text);

        this._display.setCursor(0, 0);
        this._display.print(text);
    };

    /**
     * Set the second line of text on the LCD
     *
     * @param {string} text
     */
    this.secondLine = function (text) {
        if (!this._isReady) {
            return;
        }

        text = trimString(text);
        text = addSpacesToString(text);

        this._display.setCursor(0, 1);
        this._display.print(text);
    };
};

util.inherits(Lcd, EventEmitter);

module.exports = new Lcd();
