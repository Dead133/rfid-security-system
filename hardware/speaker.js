'use strict';

var exec = require('child_process').exec;

/**
 * Class Speaker
 *
 * @constructor
 */
var Speaker = function () {

};

Speaker.prototype.playSuccessSound = function () {
    exec('./hardware/sounds/successSound', function(error, stdout, stderr) {
        void(error);
        void(stdout);
        void(stderr);
    });
};

Speaker.prototype.playErrorSound = function () {
    exec('./hardware/sounds/errorSound', function(error, stdout, stderr) {
        void(error);
        void(stdout);
        void(stderr);
    });
};

module.exports = new Speaker();
