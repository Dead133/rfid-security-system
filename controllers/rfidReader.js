/**
 * RFID reader controller
 */

'use strict';

var adminsModel = require('../models/admins');
var config = require('../config');

var cardReader;
if (config['hardware'].hardwareEnabled) {
    cardReader = require('../cardReader');
} else {
    cardReader = require('../fakeCardReader');
}

/**
 * Handler of AJAX request from dataTable with the user list
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxGetCardId(req, res) {
    if (!req.session || !req.session.admin_id) {
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            return;
        }

        var timeoutId = setTimeout(function () {
            cardReader.cancelReadForUserCrud();
            res.json({
                error: 'time_expired'
            });
        }, 10000);

        cardReader.readForUserCrud(function(error, cardId) {
            if (error) {
                res.json({
                    error: error
                });
                return;
            }

            clearTimeout(timeoutId);

            res.json({
                success: true,
                cardId: cardId
            });

            cardReader.cancelReadForUserCrud();
        });

    });
}

/**
 * RFID reader controller
 *
 * @param {Object} app - express.js application
 */
module.exports = function (app) {
    app.post('/rfidReader/get_card_id', ajaxGetCardId);
};
