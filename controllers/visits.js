/**
 * Visits controller
 */

'use strict';

var visitsModel = require('../models/visits');
var adminsModel = require('../models/admins');

/**
 * Visits page
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function visitsPage(req, res) {
    if (!req.session || !req.session.admin_id) {
        res.redirect('/login');
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            res.render('500', {
                header: '500. Internal server error',
                error: error
            });
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            req.session.destroy(function () {
                res.redirect('/login');
            });
            return;
        }

        res.render('visits', {
            activeLink: {
                visits: true
            },
            header: 'Visits',
            adminDisplayName: admin.getDisplayName(),
            jsModules: [
                'libraries/date',
                'visits'
            ]
        });
    });
}

/**
 * Handler of AJAX request from dataTable with the visit list
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxGetVisits(req, res) {
    if (!req.session || !req.session.admin_id) {
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            return;
        }

        // dataTable params
        var drawCounter = +req.body.draw;
        var start = +req.body.start;
        var limit = +req.body.length;

        visitsModel.getVisitsCount(function (error, visitsCount) {
            if (error) {
                console.error(error);
                return;
            }

            visitsModel.getVisits(start, limit, function (error, visits) {
                if (error) {
                    console.error(error);
                    return;
                }

                res.json({
                    draw: drawCounter,
                    recordsTotal: visitsCount,
                    recordsFiltered: visitsCount,
                    data: visits.serialize()
                });
            });
        });

    });
}

/**
 * Visits controller
 *
 * @param {Object} app - express.js application
 */
module.exports = function (app) {
    app.get('/visits', visitsPage);
    app.post('/visits/get_visits', ajaxGetVisits);
};
