/**
 * Monitor controller
 */

'use strict';

var adminsModel = require('../models/admins');
var visitsModel = require('../models/visits');

/**
 * Monitor page
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function monitorPage(req, res) {
    if (!req.session || !req.session.admin_id) {
        res.redirect('/login');
        return;
    }

    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            res.render('500', {
                header: '500. Internal server error',
                error: error
            });
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            req.session.destroy(function () {
                res.redirect('/login');
            });
            return;
        }

        visitsModel.getVisits(0, 5, function (error, visits) {
            if (error) {
                res.render('500', {
                    header: '500. Internal server error',
                    error: error
                });
                console.error(error);
                return;
            }

            var recentVisit = visits.length ? visits.shift() : null;

            res.render('monitor', {
                activeLink: {
                    monitor: true
                },
                header: 'Monitor',
                adminDisplayName: admin.getDisplayName(),
                jsModules: [
                    'libraries/date',
                    'monitor'
                ],
                hideHeader: true,
                recentVisit: recentVisit !== null ? recentVisit.serialize() : null,
                visits: visits.serialize()
            });
        });
    });
}

/**
 * Monitor controller
 *
 * @param {Object} app - express.js application
 */
module.exports = function (app) {
    app.get('/', monitorPage);
};
