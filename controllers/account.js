/**
 * Account controller
 */

'use strict';

var bcrypt = require('bcrypt');
var adminsModel = require('../models/admins');

/**
 * Change password page
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function changePasswordPage(req, res) {
    if (!req.session || !req.session.admin_id) {
        res.redirect('/login');
        return;
    }

    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            res.render('500', {
                header: '500. Internal server error',
                error: error
            });
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            req.session.destroy(function () {
                res.redirect('/login');
            });
            return;
        }

        res.render('changePassword', {
            header: 'Change password',
            adminDisplayName: admin.getDisplayName(),
            jsModules: [
                'libraries/input',
                'libraries/Message',
                'changePassword'
            ]
        });
    });
}

/**
 * Handler of AJAX change password request
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxChangePassword(req, res) {
    if (!req.session || !req.session.admin_id) {
        res.redirect('/login');
        return;
    }

    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            res.render('500', {
                header: '500. Internal server error',
                error: error
            });
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            req.session.destroy(function () {
                res.redirect('/login');
            });
            return;
        }

        var oldPassword = req.body['oldPassword'];
        var newPassword = req.body['newPassword'];
        var passwordConfirm = req.body['passwordConfirm'];

        if (!oldPassword || !newPassword || !passwordConfirm) {
            res.json({error: 'no_data'});
            return;
        }

        // Normalize passwords (remove whitespaces)
        oldPassword = oldPassword.trim();
        newPassword = newPassword.trim();
        passwordConfirm = passwordConfirm.trim();

        bcrypt.compare(oldPassword, admin.getPassword(), function (err, isPasswordCorrect) {
            if (!isPasswordCorrect) {
                res.json({error: 'wrong_password'});
                return;
            }

            if (newPassword !== passwordConfirm) {
                res.json({error: 'passwords_does_not_match'});
                return;
            }

            if (newPassword.length < 8) {
                res.json({error: 'password_is_too_short'});
                return;
            }

            bcrypt.hash(newPassword, 10, function(error, passwordHash) {
                if (error) {
                    res.json({error: 'internal_error'});
                    console.error(error);
                    return;
                }

                adminsModel.updateAdminPassword(admin.getId(), passwordHash, function (error) {
                    if (error) {
                        res.json({error: 'internal_error'});
                        console.error(error);
                        return;
                    }

                    res.json({success: true});
                });
            });
        });
    });
}

/**
 * Account controller
 *
 * @param {Object} app - express.js application
 */
module.exports = function (app) {
    app.get('/change_password', changePasswordPage);
    app.post('/change_password', ajaxChangePassword);
};