/**
 * Users controller
 */

'use strict';

var User = require('../entities/User');
var usersModel = require('../models/users');
var adminsModel = require('../models/admins');
var multer = require('multer');
var upload = multer({dest: '/tmp/'});
var config = require('../config');
var fsExtra = require('fs-extra');
var fs = require('fs');

/**
 * Users page
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function usersPage(req, res) {
    if (!req.session || !req.session.admin_id) {
        res.redirect('/login');
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            res.render('500', {
                header: '500. Internal server error',
                error: error
            });
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            req.session.destroy(function () {
                res.redirect('/login');
            });
            return;
        }

        res.render('users', {
            activeLink: {
                users: true
            },
            header: 'Users',
            adminDisplayName: admin.getDisplayName(),
            jsModules: [
                'libraries/Message',
                'libraries/input',
                'libraries/notify',
                'users',
                'addUser',
                'deleteUser',
                'editUser'
            ]
        });
    });
}

/**
 * Handler of AJAX request from dataTable with the user list
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxGetUsers(req, res) {
    if (!req.session || !req.session.admin_id) {
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            return;
        }

        // dataTable params
        var drawCounter = +req.body.draw;
        var start = +req.body.start;
        var limit = +req.body.length;

        usersModel.getUsersCount(function (error, usersCount) {
            if (error) {
                console.error(error);
                return;
            }

            usersModel.getUsers(start, limit, function (error, users) {
                if (error) {
                    console.error(error);
                    return;
                }

                res.json({
                    draw: drawCounter,
                    recordsTotal: usersCount,
                    recordsFiltered: usersCount,
                    data: users.serialize()
                });
            });
        });

    });
}

/**
 * Handler of AJAX request from edit user form to get user by id
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxGetUser(req, res) {
    if (!req.session || !req.session.admin_id) {
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            return;
        }

        var userId = +req.body.userId;

        usersModel.getUserById(userId, function (error, user) {
            if (error) {
                res.json({
                    error: 'internal_error'
                });
                console.error(error);
                return;
            }

            if (!user) {
                res.json({
                    error: 'user_not_found'
                });
            }

            res.json({
                success: true,
                user: user.serialize()
            });
        });

    });
}

/**
 * Handler of AJAX request to add user
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxAddUser(req, res) {
    if (!req.session || !req.session.admin_id) {
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            return;
        }

        var displayName = '' + req.body['displayName'];
        var cardId = '' + req.body['cardId'];

        if (displayName === '' || cardId === '') {
            res.json({
                error: 'empty_data'
            });
            return;
        }

        usersModel.getUserByCardId(cardId, function (error, user) {
            if (error) {
                console.error(error);
                res.json({
                    error: 'internal_error'
                });
                return;
            }

            if (user !== null) {
                res.json({
                    error: 'card_is_already_in_use',
                    cardHolderName: user.getDisplayName()
                });
                return;
            }

            if (req.file) {
                if (config['fileupload'].maxFileSize < req.file.size) {
                    res.json({
                        error: 'file_is_too_large',
                        maxFileSize: config['fileupload'].maxFileSize
                    });
                    return;
                }

                if (config['fileupload'].allowedMimeTypes.indexOf(req.file.mimetype) === -1) {
                    res.json({
                        error: 'file_type_is_not_allowed'
                    });
                    return;
                }
            }

            var hasPhoto = !!req.file;

            var newUser = new User(null, displayName, cardId, hasPhoto);

            usersModel.addUser(newUser, function (error) {
                if (error) {
                    console.error(error);
                    res.json({
                        error: 'internal_error'
                    });
                    return;
                }

                if (!hasPhoto) {
                    res.json({
                        success: true
                    });
                    return;
                }

                var tempFileName = req.file.path;
                var newFileName = './static/images/photos/user_' + newUser.getId() + '.jpg';

                fsExtra.copy(tempFileName, newFileName, function (error) {
                    if (error) {
                        console.error(error);
                        usersModel.deleteUser(newUser.getId(), function (error) {
                            if (error) {
                                console.error(error);
                            }
                        });
                        res.json({
                            error: 'internal_error'
                        });
                        return;
                    }

                    res.json({
                        success: true
                    });
                });
            });
        });

    });
}

/**
 * Handler of AJAX request to delete user
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxDeleteUser(req, res) {
    if (!req.session || !req.session.admin_id) {
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            return;
        }

        var userId = req.body['userId'];

        usersModel.getUserById(userId, function (error, user) {
            if (error) {
                console.error(error);
                res.json({
                    error: 'internal_error'
                });
                return;
            }

            if (user === null) {
                res.json({
                    error: 'user_not_found'
                });
                return;
            }

            if (!user.getHasPhoto()) {
                usersModel.deleteUser(userId, function (error) {
                    if (error) {
                        console.error(error);
                        res.json({
                            error: 'internal_error'
                        });
                        return;
                    }

                    res.json({
                        success: true
                    });
                });
                return;
            }

            var photoFileName = './static/images/photos/user_' + user.getId() + '.jpg';

            fs.stat(photoFileName, function (error, stat) {
                void(stat);
                if (error) {
                    console.error(error);
                    if (error.code !== 'ENOENT') {
                        res.json({
                            error: 'internal_error'
                        });
                    } else {
                        usersModel.deleteUser(userId, function (error) {
                            if (error) {
                                console.error(error);
                                res.json({
                                    error: 'internal_error'
                                });
                                return;
                            }

                            res.json({
                                success: true
                            });
                        });
                    }
                    return;
                }

                fs.unlink(photoFileName, function (error) {
                    if (error) {
                        console.error(error);
                        res.json({
                            error: 'internal_error'
                        });
                        return;
                    }

                    usersModel.deleteUser(userId, function (error) {
                        if (error) {
                            console.error(error);
                            res.json({
                                error: 'internal_error'
                            });
                            return;
                        }

                        res.json({
                            success: true
                        });
                    });
                });
            });
        });
    });
}

/**
 * Handler of AJAX request to update user
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxUpdateUser(req, res) {
    if (!req.session || !req.session.admin_id) {
        return;
    }
    adminsModel.getAdminById(req.session.admin_id, function (error, admin) {
        if (error) {
            console.error(error);
            return;
        }

        if (!admin) {
            console.error('Error! Cannot find admin with id ' + req.session.admin_id);
            return;
        }

        var displayName = '' + req.body['displayName'];
        var cardId = '' + req.body['cardId'];
        var userId = +req.body['id'];

        if (displayName === '' || cardId === '') {
            res.json({
                error: 'empty_data'
            });
            return;
        }

        usersModel.getUserByCardId(cardId, function (error, user) {
            if (error) {
                console.error(error);
                res.json({
                    error: 'internal_error'
                });
                return;
            }

            if (user !== null && user.getId() !== userId) {
                res.json({
                    error: 'card_is_already_in_use',
                    cardHolderName: user.getDisplayName()
                });
                return;
            }

            if (req.file) {
                if (config['fileupload'].maxFileSize < req.file.size) {
                    res.json({
                        error: 'file_is_too_large',
                        maxFileSize: config['fileupload'].maxFileSize
                    });
                    return;
                }

                if (config['fileupload'].allowedMimeTypes.indexOf(req.file.mimetype) === -1) {
                    res.json({
                        error: 'file_type_is_not_allowed'
                    });
                    return;
                }
            }

            usersModel.getUserById(userId, function (error, user) {
                if (error) {
                    console.error(error);
                    res.json({
                        error: 'internal_error'
                    });
                    return;
                }

                if (user === null) {
                    res.json({
                        error: 'user_was_not_found'
                    });
                    return;
                }

                user.setCardId(cardId);
                user.setDisplayName(displayName);

                var hasPhoto = !!req.file;

                if (!hasPhoto) {
                    usersModel.updateUser(user, function (error) {
                        if (error) {
                            console.error(error);
                            res.json({
                                error: 'internal_error'
                            });
                            return;
                        }

                        res.json({
                            success: true
                        });
                    });
                    return;
                }

                user.setHasPhoto(true);
                var tempFileName = req.file.path;
                var newFileName = './static/images/photos/user_' + user.getId() + '.jpg';

                fsExtra.copy(tempFileName, newFileName, function (error) {
                    if (error) {
                        console.error(error);
                        res.json({
                            error: 'internal_error'
                        });
                        return;
                    }

                    usersModel.updateUser(user, function (error) {
                        if (error) {
                            console.error(error);
                            res.json({
                                error: 'internal_error'
                            });
                            return;
                        }

                        res.json({
                            success: true
                        });
                    });
                });
            });
        });
    });
}

/**
 * Users controller
 *
 * @param {Object} app - express.js application
 */
module.exports = function (app) {
    app.get('/users', usersPage);
    app.post('/users/get_users', ajaxGetUsers);
    app.post('/users/get_user', ajaxGetUser);
    app.post('/users/add', upload.single('photo'), ajaxAddUser);
    app.post('/users/delete', ajaxDeleteUser);
    app.post('/users/update', upload.single('photo'), ajaxUpdateUser);
};
