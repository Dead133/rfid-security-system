/**
 * Auth controller
 */

'use strict';

var bcrypt = require('bcrypt');
var adminsModel = require('../models/admins');

/**
 * Login page
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function loginPage(req, res) {
    void(req);

    res.render('login', {
        hideNavbar: true,
        hideHeader: true,
        jsModules: [
            'libraries/input',
            'libraries/Message',
            'login'
        ]
    });
}

/**
 * Handler of AJAX login request
 *
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function ajaxLogin(req, res) {
    var login = req.body['login'];
    var password = req.body['password'];

    if (!login || !password) {
        res.json({error: 'no_data'});
        return;
    }

    // Normalize login and password (remove whitespaces, make login lowercase)
    login = login.trim().toLowerCase();
    password = password.trim();

    adminsModel.getAdminByLogin(login, function (error, admin) {
        if (error) {
            console.error(error);
            res.json({error: 'internal_error'});
            return;
        }

        if (!admin) {
            res.json({error: 'wrong_login_or_password'});
            return;
        }

        bcrypt.compare(password, admin.getPassword(), function (err, isPasswordCorrect) {
            if (!isPasswordCorrect) {
                res.json({error: 'wrong_login_or_password'});
                return;
            }

            req.session.admin_id = admin.getId();
            res.json({success: true});
        });
    });
}

/**
 * Обработчик выхода из аккаунта
 * @param {object} req - express.js request
 * @param {object} res - express.js response
 */
function logoutHandler(req, res) {
    if (req.session) {
        req.session.destroy();
    }
    res.redirect('/login');
}

/**
 * Auth controller
 *
 * @param {Object} app - express.js application
 */
module.exports = function (app) {
    app.get('/login', loginPage);
    app.post('/login', ajaxLogin);
    app.get('/logout', logoutHandler);
};