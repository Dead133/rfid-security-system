/**
 * Migrations module
 *
 * Runs independently from the project to update the database seamlessly
 *
 * TODO: Команди latest, version (з додатковим параметром -version X), back, forward
 */

'use strict';

var CURRENT_MIGRATION_FILE_NAME = './config/migration.json';
var fs = require('fs');
var databaseConnectionPool = require('./database/databaseConnectionPool');
var fileNameHelper = require('./helpers/fileNameHelper');
var currentMigration = 0;
var migrationFileNames = fs.readdirSync('./migrations/');

function saveCurrentMigrationNumber() {
    var data = {
        current: currentMigration
    };
    var json = JSON.stringify(data, null, 4);
    fs.writeFileSync(CURRENT_MIGRATION_FILE_NAME, json);
}

// Checking for some unexpected files in migrationFileNames array
migrationFileNames.forEach(function (migrationFileName, index, array) {
    var fileExtension = fileNameHelper.getFileExtension(migrationFileName);
    var fileName = fileNameHelper.getFileName(migrationFileName);

    if (!fileName || fileExtension !== 'js') {
        console.warn('Wrong file in migrations directory: ' + migrationFileName);
        array.splice(index, 1);
    }
});

migrationFileNames.sort();

if (fs.existsSync(CURRENT_MIGRATION_FILE_NAME)) {
    currentMigration = require(CURRENT_MIGRATION_FILE_NAME).current;
} else {
    saveCurrentMigrationNumber();
}

console.log('Current migration version: %s', currentMigration);

// TODO: Перевірити наявність всіх мігрейшенів в правильному порядку (без пропусків номерів), в разі відсутності якогось викинути помилку і вийти

// TODO: Перевірити, що останній нормер мігрейшена не менше за поточний, в разі невідповідності викинути помилку і вийти


// Если все миграции уже проведены - выходим.
if ( currentMigration >= migrationFileNames.length ) {
    console.log('Migrations is already up to date.');
    process.exit();
}

databaseConnectionPool.getConnection(function (error, connection) {
    if (error) {
        console.error(error);
        return;
    }

    /**
     * Проводит миграции рекурсивно
     */
    function performMigration() {
        currentMigration++;

        var migrationNumber = currentMigration.toString();
        while (migrationNumber.length < 3) {
            migrationNumber = '0' + migrationNumber;
        }
        var migrationIndex = null;
        for (var i = 0; i < migrationFileNames.length; i++) {
            var migrationFileName = migrationFileNames[i];
            if (migrationFileName.indexOf(migrationNumber) === 0) {
                migrationIndex = i;
            }
        }

        if (migrationIndex === null) {
            connection.release();
            databaseConnectionPool.end(function (error) {
                if (error) {
                    console.error(error);
                }
                process.exit();
            });
            console.error('Error! Cannot find next migration.');
            process.exit();
        }

        var migration = require('./migrations/' + migrationFileNames[ migrationIndex ]);

        console.log('Performing migration: ' + migration.description);
        migration.up(connection, function (error) {
            if (error) {
                connection.release();
                databaseConnectionPool.end(function (error) {
                    if (error) {
                        console.error(error);
                    }
                    process.exit();
                });
                console.error(error);
                process.exit();
            }

            saveCurrentMigrationNumber();
            if (currentMigration === migrationFileNames.length) {
                connection.release();
                databaseConnectionPool.end(function (error) {
                    if (error) {
                        console.error(error);
                    }
                    process.exit();
                });
            } else {
                performMigration();
            }
        });
    }

    performMigration();
});
