/**
 * Main project file, here the program execution starts
 *
 * Initialize the cardReader and creates Express.js web server
 */

'use strict';

var config = require('./config');

require('./webserver');

if (config['hardware'].hardwareEnabled) {
    require('./cardReader');
} else {
    require('./fakeCardReader');
}
