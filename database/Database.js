/**
 * Module: Database
 *
 * Grants an easy interface for querying the database
 */

'use strict';

var databaseConnectionPool = require('./databaseConnectionPool');

var Database = {};

/**
 * Performs the database query
 *
 * @param {string}                    query           Query to be performed
 * @param {[]}                        queryParameters Parameters of the query
 * @param {function(string, {}|null)} callback        Callback
 *
 * @static
 */
Database.query = function (query, queryParameters, callback) {
    if (typeof query !== 'string') {
        throw new Error('Invalid parameter type: query should be a string.');
    }
    if (!(queryParameters instanceof Array)) {
        throw new Error('Invalid parameter type: queryParameters should be an array.');
    }
    if (typeof callback !== 'function') {
        throw new Error('Invalid parameter type: callback should be a function.');
    }

    databaseConnectionPool.getConnection(function (error, connection) {
        if (error) {
            connection.release();
            callback(error, null);
            return;
        }

        connection.query(query, queryParameters, function (error, result) {
            connection.release();
            if (error) {
                callback(error, null);
                return;
            }

            callback('', result);
        });

    });
};

module.exports = Database;
