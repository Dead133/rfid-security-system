/**
 * Module: Database connection pool
 *
 * Connects to MySQL database and exports the connection pool
 */

'use strict';

var config = require('../config');
var mysql = require('mysql');

var pool = mysql.createPool({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database : config.database.databaseName
});

module.exports = pool;
