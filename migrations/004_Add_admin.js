/**
 * Migration 004
 *
 * Creates database record for the administrator
 */

'use strict';

var migration = {};

migration.description = 'Create database record for the administrator';

migration.up = function (connection, callback) {
    var query = 'INSERT INTO `admins` (login, password, display_name) VALUES (\'admin\', ' +
            '\'$2a$10$0wZxI5XOKCE84Xjs5D2r/u3H46Ot1B.5hvDbk5SCcoZG3fhd0rHWi\', \'Administrator\')';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

migration.down = function (connection, callback) {
    var query = 'DELETE FROM `admins` WHERE id = 1';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

module.exports = migration;
