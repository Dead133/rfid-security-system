/**
 * Migration 005
 *
 * Adds column 'has_photo' to table 'users'
 */

'use strict';

var migration = {};

migration.description = 'Adds column \'has_photo\' to table \'users\'';

migration.up = function (connection, callback) {
    var query = 'ALTER TABLE `users` ADD `has_photo` TINYINT(1) UNSIGNED NOT NULL;';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

migration.down = function (connection, callback) {
    var query = 'ALTER TABLE `users` DROP `has_photo`;';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

module.exports = migration;
