/**
 * Migration 003
 *
 * Creates table 'visits'
 */

'use strict';

var migration = {};

migration.description = 'Create table visits';

migration.up = function (connection, callback) {
    var query = 'CREATE TABLE `visits` (' +
        '    `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,' +
        '    `user_id` int(11) unsigned NOT NULL,' +
        '    `timestamp` int(11) unsigned NOT NULL' +
        ') ENGINE=InnoDB DEFAULT CHARSET=utf8';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

migration.down = function (connection, callback) {
    var query = 'DROP TABLE `visits`';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

module.exports = migration;
