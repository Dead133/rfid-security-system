/**
 * Migration 001
 *
 * Creates table 'users'
 */

'use strict';

var migration = {};

migration.description = 'Create table users';

migration.up = function (connection, callback) {
    var query = 'CREATE TABLE `users` (' +
        '    `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,' +
        '    `display_name` varchar(255) NOT NULL,' +
        '    `card_id` varchar(255) NOT NULL' +
        ') ENGINE=InnoDB DEFAULT CHARSET=utf8';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

migration.down = function (connection, callback) {
    var query = 'DROP TABLE `users`';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

module.exports = migration;
