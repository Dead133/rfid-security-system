/**
 * Migration 002
 *
 * Creates table 'admins'
 */

'use strict';

var migration = {};

migration.description = 'Create table admins';

migration.up = function (connection, callback) {
    var query = 'CREATE TABLE `admins` (' +
        '    `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,' +
        '    `login` varchar(255) NOT NULL,' +
        '    `password` varchar(255) NOT NULL,' +
        '    `display_name` varchar(255) NOT NULL' +
        ') ENGINE=InnoDB DEFAULT CHARSET=utf8';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

migration.down = function (connection, callback) {
    var query = 'DROP TABLE `admins`';

    connection.query(query, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

module.exports = migration;
