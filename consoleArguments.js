/**
 * Module: consoleArguments
 *
 * Returns all of the allowed for current application console arguments as object
 * Halts the application in case one of the required arguments are missing
 */

'use strict';

var ALLOWED_ARGUMENTS = {
    environment: {
        required: true
    }
};
var consoleArguments = {};
var currentArgument = null;
var nodeArguments = process.argv.slice(2);

nodeArguments.forEach(function(value, index, array) {
    void(index);
    void(array);

    if (currentArgument !== null) {
        consoleArguments[currentArgument] = value;
        currentArgument = null;
        return;
    }

    // Only accept arguments that starts with one dash and ignore Node.js arguments that starts with two
    if (value.indexOf('-') !== 0 || value.indexOf('--') === 0) {
        return;
    }

    var argument = value.substr(1);

    if (ALLOWED_ARGUMENTS.hasOwnProperty(argument)) {
        currentArgument = argument;
    } else {
        console.warn('Warning! Unknown argument:' + value);
    }
});

if (currentArgument !== null) {
    console.error('Error! Missing value for argument %s!', currentArgument);
    process.exit();
}

var argumentMissing = false;

for (var argumentName in ALLOWED_ARGUMENTS) {
    if (!ALLOWED_ARGUMENTS.hasOwnProperty(argumentName)) {
        continue;
    }

    var argument = ALLOWED_ARGUMENTS[argumentName];
    if (argument.required && !consoleArguments.hasOwnProperty(argumentName)) {
        console.error('Error! Missing required argument %s!', argumentName);
        argumentMissing = true;
    }
}

if (argumentMissing) {
    process.exit();
}

module.exports = consoleArguments;
