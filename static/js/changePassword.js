/**
 * Module: Change password
 */
$(document).ready(function () {
    'use strict';

    /**
     * Handler for change password form submit
     *
     * @param {object} event JavaScript event
     */
    function changePasswordFormSubmitHandler(event) {
        var postData = $(this).serializeArray(); // jshint ignore:line
        var formURL = $(this).attr('action'); // jshint ignore:line
        event.preventDefault();

        input.unHighlight();
        message.remove();

        $.ajax({
            url: formURL,
            type: 'POST',
            data: postData,
            success: function(data) {
                if (data.success) {
                    message.success('Password was successfully changed.');
                    return;
                }

                switch(data.error) {
                    case 'no_data':
                        if (!oldPasswordInput.val()) {
                            input.highlight(oldPasswordInput);
                        }
                        if (!newPasswordInput.val()) {
                            input.highlight(newPasswordInput);
                        }
                        if (!confirmPasswordInput.val()) {
                            input.highlight(confirmPasswordInput);
                        }
                        message.error('Please fill all the fields.');
                        break;
                    case 'wrong_password':
                        input.highlight(oldPasswordInput);
                        message.error('Wrong password.');
                        break;
                    case 'passwords_does_not_match':
                        input.highlight(newPasswordInput);
                        input.highlight(confirmPasswordInput);
                        message.error('Passwords does not match.');
                        break;
                    case 'password_is_too_short':
                        input.highlight(newPasswordInput);
                        input.highlight(confirmPasswordInput);
                        message.error('Password should be at least 8 characters long.');
                        break;
                    case 'internal_error':
                        message.error('Internal error. Please inform the administrator.');
                        break;
                    default:
                        message.error('Please try again. If this error persists, please contact ' +
                                      'the administrator.');
                }
            },
            error: function() {
                message.error('Please try again. If this error persists, please contact the ' +
                              'administrator.');
            }
        });

    }

    var message = new Message();
    var oldPasswordInput = $('#oldPasswordInput');
    var newPasswordInput = $('#newPasswordInput');
    var confirmPasswordInput = $('#confirmPasswordInput');
    var changePasswordForm = $('#jsChangePasswordForm');

    changePasswordForm.on('submit', changePasswordFormSubmitHandler);
});
