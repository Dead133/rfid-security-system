/**
 * Library: notify
 * Simple wrapper for third-party notification library
 */
(function (window) {
    'use strict';

    var notify = {};

    notify.success = function(message) {
        $.notify({
            // options
            message: message
        }, {
            // settings
            type: 'success',
            offset: {
                x: 10,
                y: 60
            }
        });
    };

    notify.error = function(message) {
        $.notify({
            // options
            message: 'Error! ' + message
        }, {
            // settings
            type: 'danger',
            offset: {
                x: 10,
                y: 60
            }
        });
    };

    window.notify = notify;
})(window);
