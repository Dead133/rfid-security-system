/**
 * Library: Message
 * OOP wrapper for dynamic showing Bootstrap messages with JavaScript
 * Requires jQuery
 * Dependency: Object.create shim for old browsers
 */
(function (window) {
    'use strict';

    var MESSAGE_COLOR_RED = 'danger';
    var MESSAGE_COLOR_GREEN = 'success';
    var MESSAGE_COLOR_BLUE = 'info';
    var MESSAGE_COLOR_YELLOW = 'warning';

    /**
     * Message constructor
     *
     * @param {object} [messageContainer] jQuery-selector, element that would contain the message
     * @param {string} [style] Additional CSS styles for the message element
     */
    var Message = function (messageContainer, style) {
        var self = this instanceof Message ? this : Object.create(Message.prototype);
        style = typeof style === 'undefined' ? '' : String(style);
        messageContainer = typeof messageContainer === 'undefined' ? $('#jsMessageContainer') :
                           messageContainer;

        /**
         * Basic function for showing the message
         *
         * @param {string} messageText  Message text
         * @param {string} messageColor Message color
         */
        function showMessage(messageText, messageColor) {
            var messageHTML = [
                '        <div class="alert alert-' + messageColor + ' fade in jsMessage" style="' +
                this.style + '">', // jshint ignore:line
                '            <button type="button" class="close" data-dismiss="alert"' +
                ' aria-hidden="true">×</button>',
                '            ' + messageText,
                '        </div>'
            ].join('\n');
            messageContainer.html(messageHTML);
        }

        /**
         * Show error message (red, with bold "Error!" caption before the text)
         *
         * @param {string} messageText Text of the message
         */
        self.error = function (messageText) {
            var text = '<strong>Error!</strong> ' + messageText;
            showMessage.call(this, text, MESSAGE_COLOR_RED);
        };

        /**
         * Show success message (green)
         *
         * @param {string} messageText Text of the message
         */
        self.success = function (messageText) {
            showMessage.call(this, messageText, MESSAGE_COLOR_GREEN);
        };

        /**
         * Show info message (blue)
         *
         * @param {string} messageText Text of the message
         */
        self.info = function (messageText) {
            var text = '<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> ' +
                       messageText;
            showMessage.call(this, text, MESSAGE_COLOR_BLUE);
        };

        /**
         * Show warning message (yellow)
         *
         * @param {string} messageText Text of the message
         */
        self.warn = function (messageText) {
            showMessage.call(this, messageText, MESSAGE_COLOR_YELLOW);
        };

        /**
         * Removes all messages from the DOM
         */
        self.remove = function () {
            messageContainer.empty();
        };

        return self;
    };

    window.Message = Message;
})(window);
