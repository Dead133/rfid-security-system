/**
 * Library: Date
 * Simple set of function that could be used to convert timestamp to pretty formatted string
 */

var date = {};

/**
 * Converts UNIX timestamp to a formatted string date in dd.mm.yyyy format (UTC timezone)
 *
 * @param {int} timestamp UNIX timestamp in milliseconds
 */
date.getUTCDateString = function (timestamp) {
    'use strict';
    var date = new Date(timestamp);
    var year = date.getUTCFullYear();
    var monthNumber = date.getUTCMonth() + 1;
    var month = monthNumber.toString().length === 1 ? '0' + monthNumber : monthNumber;
    var day = date.getUTCDate().toString().length === 1 ? '0' + date.getUTCDate() :
              date.getUTCDate();
    return day + '.' + month + '.' + year;
};

/**
 * Converts UNIX timestamp to a formatted string date in dd.mm.yyyy hh:mm:ss format (UTC timezone)
 *
 * @param {int} timestamp UNIX timestamp in milliseconds
 */
date.getUTCDateTimeString = function (timestamp) {
    'use strict';
    var date = new Date(timestamp);
    var hours = date.getUTCHours().toString().length === 1 ? '0' + date.getUTCHours() :
                date.getUTCHours();
    var minutes = date.getUTCMinutes().toString().length === 1 ? '0' + date.getUTCMinutes() :
                  date.getUTCMinutes();
    var seconds = date.getUTCSeconds().toString().length === 1 ? '0' + date.getUTCSeconds() :
                  date.getUTCSeconds();
    var year = date.getUTCFullYear();
    var monthNumber = date.getUTCMonth() + 1;
    var month = monthNumber.toString().length === 1 ? '0' + monthNumber : monthNumber;
    var day = date.getUTCDate().toString().length === 1 ? '0' + date.getUTCDate() :
              date.getUTCDate();
    return day + '.' + month + '.' + year + ' ' + hours + ':' + minutes + ':' + seconds;
};

/**
 * Converts UNIX timestamp to a formatted string date in dd.mm.yyyy hh:mm format (UTC timezone)
 *
 * @param {int} timestamp UNIX timestamp in milliseconds
 */
date.getUTCDateHoursMinutesString = function (timestamp) {
    'use strict';
    var date = new Date(timestamp);
    var hours = date.getUTCHours().toString().length === 1 ? '0' + date.getUTCHours() :
                date.getUTCHours();
    var minutes = date.getUTCMinutes().toString().length === 1 ? '0' + date.getUTCMinutes() :
                  date.getUTCMinutes();
    var year = date.getUTCFullYear();
    var monthNumber = date.getUTCMonth() + 1;
    var month = monthNumber.toString().length === 1 ? '0' + monthNumber : monthNumber;
    var day = date.getUTCDate().toString().length === 1 ? '0' + date.getUTCDate() :
              date.getUTCDate();
    return day + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
};

/**
 * Converts UNIX timestamp to a formatted string date in dd.mm.yyyy format
 *
 * @param {int} timestamp UNIX timestamp in milliseconds
 */
date.getDateString = function (timestamp) {
    'use strict';
    var date = new Date(timestamp);
    var year = date.getFullYear();
    var monthNumber = date.getMonth() + 1;
    var month = monthNumber.toString().length === 1 ? '0' + monthNumber : monthNumber;
    var day = date.getDate().toString().length === 1 ? '0' + date.getDate() : date.getDate();
    return day + '.' + month + '.' + year;
};

/**
 * Converts UNIX timestamp to a formatted string date in dd.mm.yyyy hh:mm:ss format
 *
 * @param {int} timestamp UNIX timestamp in milliseconds
 */
date.getDateTimeString = function (timestamp) {
    'use strict';
    var date = new Date(timestamp);
    var hours = date.getHours().toString().length === 1 ? '0' + date.getHours() : date.getHours();
    var minutes = date.getMinutes().toString().length === 1 ? '0' + date.getMinutes() :
                  date.getMinutes();
    var seconds = date.getSeconds().toString().length === 1 ? '0' + date.getSeconds() :
                  date.getSeconds();
    var year = date.getFullYear();
    var monthNumber = date.getMonth() + 1;
    var month = monthNumber.toString().length === 1 ? '0' + monthNumber : monthNumber;
    var day = date.getDate().toString().length === 1 ? '0' + date.getDate() : date.getDate();

    return day + '.' + month + '.' + year + ' ' + hours + ':' + minutes + ':' + seconds;
};

/**
 * Converts UNIX timestamp to a formatted string date in dd.mm.yyyy hh:mm format
 *
 * @param {int} timestamp UNIX timestamp in milliseconds
 */
date.getDateHoursMinutesString = function (timestamp) {
    'use strict';
    var date = new Date(timestamp);
    var hours = date.getHours().toString().length === 1 ? '0' + date.getHours() : date.getHours();
    var minutes = date.getMinutes().toString().length === 1 ? '0' + date.getMinutes() :
                  date.getMinutes();
    var year = date.getFullYear();
    var monthNumber = date.getMonth() + 1;
    var month = monthNumber.toString().length === 1 ? '0' + monthNumber : monthNumber;
    var day = date.getDate().toString().length === 1 ? '0' + date.getDate() :
              date.getDate();
    return day + '.' + month + '.' + year + ' ' + hours + ':' + minutes;
};
