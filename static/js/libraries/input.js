/**
 * Library: input
 */

var input = {}; // jshint ignore:line

/**
 * Highlight an input field with the red border, informing user about wrong value of the field
 * @param {object} inputElement jQuery DOM element selector
 */
input.highlight = function (inputElement) {
    'use strict';
    inputElement.parents('.form-group').addClass('has-error');
};

/**
 * Removes highlight from all inputs on the page
 */
input.unHighlight = function () {
    'use strict';
    $('.has-error').removeClass('has-error');
};
