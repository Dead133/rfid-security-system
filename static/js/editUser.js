$(document).ready(function () {
    'use strict';

    var editUserModal = $('#editUserModal');
    var editUserModalMessageContainer = $('#editUserModalMessageContainer');
    var editUserModalMessage = new Message(editUserModalMessageContainer);
    var editUserForm = $('#editUserForm');
    var userIdInput = $('#editUserModalUserIdInput');
    var userNameInput = $('#editUserModalUserNameInput');
    var userCardIdInput = $('#editUserModalUserCardIdInput');
    var userPhotoInput = $('#editUserModalUserPhotoInput');
    var editUserModalLabel = $('#editUserModalLabel');
    var readFromCardReaderLink = $('#editUserModalReadFromCardReaderLink');
    var readFromCardReaderLinkCounterIntervalId = null;
    var readFromCardReaderLinkCounterValue = 0;

    editUserModal.modal({
        show: false
    });

    /**
     * On show handler for add user modal
     *
     * @param {object} event JavaScript event
     */
    function editUserModalShownHandler(event) {
        void(event);

        userNameInput.focus();
    }

    /**
     * "Add user" button click event handler
     *
     * @param {object} event JavaScript event
     */
    function editUserButtonClickHandler(event) {
        var userName = $(this).data('user-display-name'); // jshint ignore:line
        var userId = +$(this).data('user-id'); // jshint ignore:line

        getUserDataFromServer(userId, function (error, user) {
            if (error) {
                switch (error) {
                    case 'internal_error':
                        notify.error('Internal error. Please inform the administrator.');
                        break;
                    case 'user_not_found':
                        notify.error('User was not found');
                        break;
                    case 'request_failed':
                        notify.error('Please try again. If this error persists, ' +
                                     'please contact the administrator.');
                        break;
                    default:
                        notify.error('Please try again. If this error persists, ' +
                                     'please contact the administrator.');
                }
                return;
            }

            editUserModalMessage.remove();
            userIdInput.val(user.id);
            userNameInput.val(user.displayName);
            userCardIdInput.val(user.cardId);
            userPhotoInput.val('');
            input.unHighlight();

            editUserModalLabel.text('Edit user "' + userName + '"');

            editUserModal.modal('show');
        });

        event.preventDefault();
    }

    /**
     * Add user form submit handler
     *
     * @param {object} event JavaScript event
     */
    function editUserFormSubmitHandler(event) {
        var postData = new FormData(this); // jshint ignore:line
        var formURL = $(this).attr('action'); // jshint ignore:line
        event.preventDefault();

        input.unHighlight();
        editUserModalMessage.remove();

        $.ajax({
            url: formURL,
            type: 'POST',
            data: postData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.success) {
                    editUserModal.modal('hide');
                    notify.success('User "' + userNameInput.val() + '" was successfully saved.');
                    window.usersDataTable.fnDraw();
                    return;
                }

                switch (data.error) {
                    case 'empty_data':
                        if (!userNameInput.val()) {
                            input.highlight(userNameInput);
                        }
                        if (!userCardIdInput.val()) {
                            input.highlight(userCardIdInput);
                        }
                        editUserModalMessage.error('Please enter valid data.');
                        break;
                    case 'internal_error':
                        editUserModalMessage.error('Internal error. Please inform the ' +
                                                   'administrator.');
                        break;
                    case 'card_is_already_in_use':
                        var cardHolderName = data['cardHolderName'];
                        editUserModalMessage.error('This card id is already used by user "' +
                                                   cardHolderName + '".');
                        break;
                    case 'user_was_not_found':
                        editUserModalMessage.error('User was no found. Have you deleted this ' +
                                                   'user? If not, contact the administrator.');
                        break;
                    default:
                        editUserModalMessage.error('Please try again. If this error persists, ' +
                                                   'please contact the administrator.');
                }
            },
            error: function () {
                editUserModalMessage.error('Please try again. If this error persists, please ' +
                                           'contact the administrator.');
            }
        });
    }

    /**
     * Retrieves user data from the server
     *
     * @param {int}                      userId   User database record id
     * @param {function(string, object)} callback Callback(error, user)
     */
    function getUserDataFromServer(userId, callback) {
        $.ajax({
            url: '/users/get_user',
            type: 'POST',
            data: {
                userId: userId
            },
            success: function (data) {
                if (data.success) {
                    callback('', data['user']);
                    return;
                }

                callback(data.error, null);
            },
            error: function () {
                callback('request_failed', null);
            }
        });
    }

    /**
     * readFromCardReaderLink click handler
     *
     * @param {object} event JavaScript event
     */
    function readFromCardReaderLinkClickHandler(event) {
        readFromCardReaderLink.addClass('disabled');
        event.preventDefault();

        readFromCardReaderLinkCounterValue = 10;
        readFromCardReaderLink.text('Waiting, time left: ' + readFromCardReaderLinkCounterValue);
        readFromCardReaderLinkCounterIntervalId = setInterval(function() {
            if (readFromCardReaderLinkCounterValue <= 0) {
                return;
            }
            readFromCardReaderLinkCounterValue--;
            readFromCardReaderLink.text('Waiting, time left: ' + readFromCardReaderLinkCounterValue);

        }, 1000);

        $.ajax({
            url: '/rfidReader/get_card_id',
            type: 'POST',
            data: {},
            success: function (data) {
                clearInterval(readFromCardReaderLinkCounterIntervalId);
                readFromCardReaderLink.text('Read from card reader');
                readFromCardReaderLink.removeClass('disabled');
                if (data.success) {
                    userCardIdInput.val(data.cardId);
                    return;
                }

                switch (data.error) {
                    case 'already_in_use_for_user_crud':
                        editUserModalMessage.error('Card reader is in use by another user.');
                        break;
                    case 'time_expired':
                        break;
                    default:
                        editUserModalMessage.error('Please try again. If this error persists, ' +
                                                  'please contact the administrator.');
                }
            },
            error: function () {
                clearInterval(readFromCardReaderLinkCounterIntervalId);
                readFromCardReaderLink.text('Read from card reader');
                readFromCardReaderLink.removeClass('disabled');
                editUserModalMessage.error('Please try again. If this error persists, please ' +
                                          'contact the administrator.');
            }
        });
    }

    window.usersDataTable.on('click touchstart', '.jsEditUser', editUserButtonClickHandler);
    editUserForm.on('submit', editUserFormSubmitHandler);
    editUserModal.on('shown.bs.modal', editUserModalShownHandler);
    readFromCardReaderLink.on('click touchstart', readFromCardReaderLinkClickHandler);
});
