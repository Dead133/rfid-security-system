$(document).ready(function () {
    'use strict';

    var tableElement = $('#dataTable');

    window.usersDataTable = tableElement.dataTable({
        responsive: true,
        autoWidth: true,
        searching: false,
        lengthChange: false,
        pageLength: 10,
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: function (data, callback) {
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/users/get_users',
                data: data,
                success: callback
            });
        },
        columns: [
            {
                data: 'id',
                class: 'dataTableCell'
            },
            {
                data: 'displayName',
                class: 'dataTableCell'
            },
            {
                data: 'cardId',
                class: 'dataTableCell'
            },
            {
                data: 'id',
                class: 'text-center dataTableCell',
                width: '1%',
                render: function (data, type, full, meta) {
                    void(type);
                    void(meta);

                    return '<a class="btn btn-sm btn-success jsShowUserVisitsLog disabled"' +
                           '        href="#" data-user-id="' + data + '"' +
                           '        data-user-display-name="' + full['displayName'] + '"' +
                           '        data-toggle="tooltip" data-placement="top"' +
                           '        title="Show users visit log">' +
                           '    <i class="glyphicon glyphicon-calendar"></i>' +
                           '</a>' +
                           '&nbsp;' +
                           '<a class="btn btn-sm btn-warning jsEditUser"' +
                           '        href="#" data-user-id="' + data + '"' +
                           '        data-user-display-name="' + full['displayName'] + '"' +
                           '        data-toggle="tooltip" data-placement="top"' +
                           '        title="Edit user">' +
                           '    <i class="glyphicon glyphicon-edit"></i>' +
                           '</a>' +
                           '&nbsp;' +
                           '<a class="btn btn-sm btn-danger jsDeleteUser"' +
                           '        href="#" data-user-id="' + data + '"' +
                           '        data-user-display-name="' + full['displayName'] + '"' +
                           '        data-toggle="tooltip" data-placement="top"' +
                           '        title="Delete user">' +
                           '    <i class="glyphicon glyphicon-remove"></i>' +
                           '</a>';
                }
            }
        ]
    });
});
