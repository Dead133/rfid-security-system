/**
 * Module: Monitor
 */
$(document).ready(function () {
    'use strict';

    var noVisitsSoFarCaption = $('#noVisitsSoFarCaption');
    var recentVisitPanel = $('#recentVisitPanel');
    var resentVisitUserNameSpan = $('#resentVisitUserName');
    var resentVisitUserCardIdSpan = $('#resentVisitUserCardId');
    var resentVisitUserPhoto = $('#resentVisitUserPhoto');
    var visits = $('#visits');

    socket.on('visit', function (visit) {
        noVisitsSoFarCaption.addClass('hidden');

        var recentVisit = getRecentVisit();
        setRecentVisit(visit);
        addVisit(recentVisit);
    });

    $('.timestamp, #resentVisitTime').each(function (key, value) {
        void(key);
        void(value);
        var timestamp = +$(this).text();
        $(this).text(date.getDateTimeString(timestamp * 1000));
    });


    function getRecentVisit() {
        if (recentVisitPanel.hasClass('hidden')) {
            return null;
        }

        var visit = {};

        visit.id = recentVisitPanel.data('visitId');
        visit.timestamp = +recentVisitPanel.data('timestamp');

        if (!recentVisitPanel.data('userId')) {
            visit.user = null;
        } else {
            visit.user = {};
            visit.user.id = recentVisitPanel.data('userId');
            visit.user.displayName = resentVisitUserNameSpan.text().trim();
            visit.user.cardId = resentVisitUserCardIdSpan.text().trim();
            visit.user.hasPhoto =
                    resentVisitUserPhoto.attr('src').trim() !== '/images/user_default.png';
        }

        return visit;
    }

    function setRecentVisit(visit) {
        if (recentVisitPanel.hasClass('hidden')) {
            recentVisitPanel.removeClass('hidden');
        }

        recentVisitPanel.data('visitId', visit.id);
        recentVisitPanel.data('timestamp', visit.timestamp);
        recentVisitPanel.data('userId', visit.user !== null ? visit.user.id : '');
        resentVisitUserNameSpan.text(visit.user !== null ? visit.user.displayName :
                                     '[USER REMOVED]');
        resentVisitUserCardIdSpan.text(visit.user !== null ? visit.user.cardId : '[USER REMOVED]');
        $('#resentVisitTime').text(date.getDateTimeString(visit.timestamp));

        var photo = visit.user !== null && visit.user.hasPhoto ?
                    '/images/photos/user_' + visit.user.id + '.jpg' :
                    '/images/user_default.png';
        resentVisitUserPhoto.attr('src', photo);
    }

    function addVisit(visit) {
        if (visits.children().length >= 4) {
            visits.children().last().remove();
        }

        var photo = visit.user !== null && visit.user.hasPhoto ?
                    '/images/photos/user_' + visit.user.id + '.jpg' :
                    '/images/user_default.png';

        var displayName = visit.user !== null ? visit.user.displayName : '[USER REMOVED]';
        var cardId = visit.user !== null ? visit.user.cardId : '[USER REMOVED]';
        var html = [
            '<div class="panel panel-default" style="padding: 5px; margin-bottom: 5px;">',
            '    <div class="panel-body" style="padding: 5px;">',
            '        <div class="row">',
            '            <div class="col-sm-2" style="padding-left: 20px; padding-right: 20px;">',
            '                <img src="' + photo + '" class="thumbnail" alt="..."',
            '                        style="width: 100%; margin-bottom: 0;">',
            '            </div>',
            '            <div class="col-sm-10">',
            '                <p class="text-center">',
            '                    ' + displayName,
            '                </p>',
            '                <p class="text-center">',
            '                    Card id: ' + cardId,
            '                </p>',
            '                <p class="text-center">',
            '                    Time: ' + date.getDateTimeString(visit.timestamp * 1000),
            '                </p>',
            '            </div>',
            '        </div>',
            '    </div>',
            '</div>'
        ].join('\n');
        visits.prepend(html);
    }
});
