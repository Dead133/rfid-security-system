$(document).ready(function () {
    'use strict';

    /**
     * "Delete user" button click event handler
     *
     * @param {object} event JavaScript event
     */
    function deleteUserButtonClickHandler(event) {
        var userId = $(this).data('user-id'); // jshint ignore:line
        var userDisplayName = $(this).data('user-display-name'); // jshint ignore:line

        event.preventDefault();

        var message = 'Are you sure you want to delete user ' + userDisplayName + '?';
        bootbox.confirm(message, function (isConfirmed) {
            if (!isConfirmed) {
                return;
            }

            $.ajax({
                url: '/users/delete',
                type: 'POST',
                data: {
                    userId: userId
                },
                success: function (data) {
                    if (data.success) {
                        notify.success('User "' + userDisplayName + '" was successfully deleted.');
                        window.usersDataTable.fnDraw();
                        return;
                    }

                    switch (data.error) {
                        case 'user_not_found':
                            notify.error('Cannot find user with id "' + userId + '"!');
                            break;
                        case 'internal_error':
                            notify.error('Internal error. Please inform the administrator.');
                            break;
                        default:
                            notify.error('Please try again. If this error persists, please ' +
                                         'contact the administrator.');
                    }
                },
                error: function () {
                    notify.error('Please try again. If this error persists, please ' +
                                 'contact the administrator.');
                }
            });
        });
    }

    window.usersDataTable.on('click touchstart', '.jsDeleteUser', deleteUserButtonClickHandler);
});
