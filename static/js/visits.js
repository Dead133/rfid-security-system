$(document).ready(function () {
    'use strict';

    var tableElement = $('#dataTable');

    tableElement.dataTable({
        responsive: true,
        autoWidth: true,
        searching: false,
        lengthChange: false,
        pageLength: 10,
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: function (data, callback) {
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: '/visits/get_visits',
                data: data,
                success: callback
            });
        },
        columns: [
            {
                data: 'id',
                class: 'dataTableCell'
            },
            {
                data: 'timestamp',
                class: 'dataTableCell',
                render: function (data, type, full, meta) {
                    void(type);
                    void(full);
                    void(meta);

                    return date.getDateTimeString(data * 1000);
                }
            },
            {
                data: 'user',
                class: 'dataTableCell',
                render: function (data, type, full, meta) {
                    void(type);
                    void(full);
                    void(meta);

                    if (data === null) {
                        return '[USER REMOVED]';
                    }

                    return data.displayName;
                }
            },
            {
                data: 'user',
                class: 'dataTableCell',
                render: function (data, type, full, meta) {
                    void(type);
                    void(full);
                    void(meta);

                    if (data === null) {
                        return '[USER REMOVED]';
                    }

                    return data.cardId;
                }
            }
        ]
    });
});
