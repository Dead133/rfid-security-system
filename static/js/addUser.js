$(document).ready(function () {
    'use strict';

    var addUserButton = $('#addUserButton');
    var addUserModal = $('#addUserModal');
    var addUserModalMessageContainer = $('#addUserModalMessageContainer');
    var addUserModalMessage = new Message(addUserModalMessageContainer);
    var addUserForm = $('#addUserForm');
    var userNameInput = $('#userNameInput');
    var userCardIdInput = $('#userCardIdInput');
    var userPhotoInput = $('#userPhotoInput');
    var readFromCardReaderLink = $('#readFromCardReaderLink');
    var readFromCardReaderLinkCounterIntervalId = null;
    var readFromCardReaderLinkCounterValue = 0;

    addUserModal.modal({
        show: false
    });

    /**
     * On show handler for add user modal
     *
     * @param {object} event JavaScript event
     */
    function addUserModalShownHandler(event) {
        void(event);
        userNameInput.focus();
    }

    /**
     * "Add user" button click event handler
     *
     * @param {object} event JavaScript event
     */
    function addUserButtonClickHandler(event) {
        event.preventDefault();

        addUserModalMessage.remove();
        userNameInput.val('');
        userCardIdInput.val('');
        userPhotoInput.val('');
        input.unHighlight();

        addUserModal.modal('show');
    }

    /**
     * Add user form submit handler
     *
     * @param {object} event JavaScript event
     */
    function addUserFormSubmitHandler(event) {
        var postData = new FormData(this); // jshint ignore:line
        var formURL = $(this).attr('action'); // jshint ignore:line
        event.preventDefault();

        input.unHighlight();
        addUserModalMessage.remove();

        $.ajax({
            url: formURL,
            type: 'POST',
            data: postData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.success) {
                    addUserModal.modal('hide');
                    notify.success('User "' + userNameInput.val() + '" was successfully saved.');
                    window.usersDataTable.fnDraw();
                    return;
                }

                switch (data.error) {
                    case 'empty_data':
                        if (!userNameInput.val()) {
                            input.highlight(userNameInput);
                        }
                        if (!userCardIdInput.val()) {
                            input.highlight(userCardIdInput);
                        }
                        addUserModalMessage.error('Please enter valid data.');
                        break;
                    case 'internal_error':
                        addUserModalMessage.error('Internal error. Please inform the administrator.');
                        break;
                    case 'card_is_already_in_use':
                        var cardHolderName = data['cardHolderName'];
                        addUserModalMessage.error('This card id is already used by user "' +
                                                  cardHolderName + '".');
                        break;
                    default:
                        addUserModalMessage.error('Please try again. If this error persists, ' +
                                                  'please contact the administrator.');
                }
            },
            error: function () {
                addUserModalMessage.error('Please try again. If this error persists, please ' +
                                          'contact the administrator.');
            }
        });

    }

    /**
     * readFromCardReaderLink click handler
     *
     * @param {object} event JavaScript event
     */
    function readFromCardReaderLinkClickHandler(event) {
        readFromCardReaderLink.addClass('disabled');
        event.preventDefault();

        readFromCardReaderLinkCounterValue = 10;
        readFromCardReaderLink.text('Waiting, time left: ' + readFromCardReaderLinkCounterValue);
        readFromCardReaderLinkCounterIntervalId = setInterval(function() {
            if (readFromCardReaderLinkCounterValue <= 0) {
                return;
            }
            readFromCardReaderLinkCounterValue--;
            readFromCardReaderLink.text('Waiting, time left: ' + readFromCardReaderLinkCounterValue);

        }, 1000);

        $.ajax({
            url: '/rfidReader/get_card_id',
            type: 'POST',
            data: {},
            success: function (data) {
                clearInterval(readFromCardReaderLinkCounterIntervalId);
                readFromCardReaderLink.text('Read from card reader');
                readFromCardReaderLink.removeClass('disabled');
                if (data.success) {
                    userCardIdInput.val(data.cardId);
                    return;
                }

                switch (data.error) {
                    case 'already_in_use_for_user_crud':
                        addUserModalMessage.error('Card reader is in use by another user.');
                        break;
                    case 'time_expired':
                        break;
                    default:
                        addUserModalMessage.error('Please try again. If this error persists, ' +
                                                  'please contact the administrator.');
                }
            },
            error: function () {
                clearInterval(readFromCardReaderLinkCounterIntervalId);
                readFromCardReaderLink.text('Read from card reader');
                readFromCardReaderLink.removeClass('disabled');
                addUserModalMessage.error('Please try again. If this error persists, please ' +
                                          'contact the administrator.');
            }
        });
    }

    addUserButton.on('click touchstart', addUserButtonClickHandler);
    addUserForm.on('submit', addUserFormSubmitHandler);
    addUserModal.on('shown.bs.modal', addUserModalShownHandler);
    readFromCardReaderLink.on('click touchstart', readFromCardReaderLinkClickHandler);
});
