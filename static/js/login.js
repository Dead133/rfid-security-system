/**
 * Module: Login
 */
$(document).ready(function () {
    'use strict';

    /**
     * Handler for login form submit
     *
     * @param {object} event JavaScript event
     */
    function loginFormSubmitHandler(event) {
        var postData = $(this).serializeArray(); // jshint ignore:line
        var formURL = $(this).attr('action'); // jshint ignore:line
        event.preventDefault();

        input.unHighlight();
        message.remove();

        $.ajax({
            url: formURL,
            type: 'POST',
            data: postData,
            success: function(data) {
                if (data.success) {
                    $(location).attr('href', '/');
                    return;
                }

                switch(data.error) {
                    case 'no_data':
                        if (!loginInput.val()) {
                            input.highlight(loginInput);
                        }
                        if (!passwordInput.val()) {
                            input.highlight(passwordInput);
                        }
                        message.error('Please enter your login and password.');
                        break;
                    case 'wrong_login_or_password':
                        input.highlight(loginInput);
                        input.highlight(passwordInput);
                        message.error('Wrong login or password.');
                        break;
                    case 'internal_error':
                        message.error('Internal error. Please inform the administrator.');
                        break;
                    default:
                        message.error('Please try again. If this error persists, please contact ' +
                                      'the administrator.');
                }
            },
            error: function() {
                message.error('Please try again. If this error persists, please contact the ' +
                              'administrator.');
            }
        });

    }


    var message = new Message();
    var loginInput = $('#loginInput');
    var passwordInput = $('#passwordInput');
    var loginForm = $('#jsLoginForm');

    loginForm.on('submit', loginFormSubmitHandler);
});
