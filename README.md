# README

## Before install

Don't forget to disable Raspberry Pi serial port login and bootup info: [tutorial](http://www.hobbytronics.co.uk/raspberry-pi-serial-port)

## How to run

    sudo /home/pi/.bin/node-v0.10.28-linux-arm-pi/bin/node /home/pi/rfid-security-system/index.js -environment production

## How to setup the autorun
 
Add the following line to ```/etc/rc.local``` before the ```exit 0``` line
 
    /home/pi/.bin/node-v0.10.28-linux-arm-pi/bin/node /home/pi/rfid-security-system/index.js  -environment production < /dev/null &

## Access data

1. Raspberry Pi ip: 192.168.0.99

1. Phpmyadmin  
url: http://192.168.0.99:8000/phpmyadmin/  
user/password: root/rpirfidlol

1. SSH user/password: pi/rpirfidlol

1. Mysql user/password: root/rpirfidlol

Available card ids:  
1D00118D53D2  
1D00118C9414  
1D00118DD150
