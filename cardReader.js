var Led = require('./hardware/Led');
var rfidReader = require('./hardware/rfidReader');
var lcd = require('./hardware/lcd');
var speaker = require('./hardware/speaker');
var greenLed = new Led(12);
var yellowLed = new Led(16);
var readyStatusLed = new Led(18);
var isRFIDReaderActive = true;
var Visit = require('./entities/Visit');
var visitsModel = require('./models/visits');
var usersModel = require('./models/users');
var EventEmitter = require('events').EventEmitter;
var util = require('util');

var CardReader = function () {
    'use strict';

    var self = this;
    var isInReadForUserCrudMode = false;
    var readForUserCrudCallback = null;

    /**
     * Turns the rfid reader into "Read for the user CRUD" mode
     *
     * @param {function(string, string)} callback
     */
    self.readForUserCrud = function (callback) {
        if (isInReadForUserCrudMode) {
            callback('already_in_use_for_user_crud', null);
        }

        readForUserCrudCallback = callback;
        isInReadForUserCrudMode = true;
    };

    /**
     * Turns off the "Read for the user CRUD" mode
     */
    self.cancelReadForUserCrud = function () {
        isInReadForUserCrudMode = false;
        readForUserCrudCallback = null;
    };

    /**
     * Turns on the green status LED
     */
    self.turnOnReadyLed = function () {
        readyStatusLed.turnOn();
    };

    lcd.on('ready', function () {
        if (!isRFIDReaderActive) {
            return;
        }
        lcd.firstLine('Swipe your card');
    });

    rfidReader.on('error', function (error) {
        console.error(error);
    });

    rfidReader.on('data', function (data) {
        if (!isRFIDReaderActive) {
            return;
        }

        if (isInReadForUserCrudMode) {
            readForUserCrudCallback('', data);
            lcd.firstLine('Card id was read');
            speaker.playSuccessSound();
            isRFIDReaderActive = false;
            setTimeout(function () {
                lcd.firstLine('Swipe your card');
                isRFIDReaderActive = true;
            }, 3000);
            return;
        }

        usersModel.getUserByCardId(data, function (error, user) {
            if (error) {
                console.error(error);
                return;
            }

            if (user === null) {
                console.log('Access denied.');
                console.log('Card id: ' + data + '\n');
                yellowLed.turnOn();
                lcd.firstLine('Access denied');
                speaker.playErrorSound();
                isRFIDReaderActive = false;
                setTimeout(function () {
                    yellowLed.turnOff();
                    lcd.firstLine('Swipe your card');
                    isRFIDReaderActive = true;
                }, 3000);
                self.emit('access_denied');
                return;
            }

            console.log('Access granted:');
            console.log('User id: ' + user.getId());
            console.log('User name: ' + user.getDisplayName());
            console.log('User card id: ' + user.getCardId() + '\n');
            greenLed.turnOn();
            lcd.firstLine('Access granted');
            speaker.playSuccessSound();
            isRFIDReaderActive = false;
            setTimeout(function () {
                greenLed.turnOff();
                lcd.firstLine('Swipe your card');
                isRFIDReaderActive = true;
            }, 3000);
            var timestamp = Date.now();
            var visit = new Visit(null, user, timestamp);
            visitsModel.addVisit(visit, function (error) {
                if (error) {
                    console.error(error);
                    return;
                }

                self.emit('visit', visit);
            });
        });

    });
};

util.inherits(CardReader, EventEmitter);

module.exports = new CardReader();
