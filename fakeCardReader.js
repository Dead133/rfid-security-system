var Visit = require('./entities/Visit');
var visitsModel = require('./models/visits');
var usersModel = require('./models/users');
var fs = require('fs');
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var fileName = 'fakeCardReader.txt';

var FakeCardReader = function () {
    'use strict';

    var self = this;
    var isInReadForUserCrudMode = false;
    var readForUserCrudCallback = null;

    /**
     * Turns the rfid reader into "Read for the user CRUD" mode
     *
     * @param {function(string, string)} callback
     */
    self.readForUserCrud = function (callback) {
        if (isInReadForUserCrudMode) {
            callback('already_in_use_for_user_crud', null);
        }

        readForUserCrudCallback = callback;
        isInReadForUserCrudMode = true;
    };

    /**
     * Turns off the "Read for the user CRUD" mode
     */
    self.cancelReadForUserCrud = function () {
        isInReadForUserCrudMode = false;
        readForUserCrudCallback = null;
    };

    /**
     * Turns on the green status LED
     */
    self.turnOnReadyLed = function () {
        // Do nothing
    };

    fs.watchFile(fileName, function (current, previous) {
        "use strict";

        void(current);
        void(previous);

        fs.readFile(fileName, 'utf8', function (error, data) {
            if (error) {
                console.error('Error reading fakeCardReader.txt: ' + error);
                return;
            }

            if (data === '') {
                return;
            }

            fs.truncate(fileName, 0, function () {
                if (isInReadForUserCrudMode) {
                    readForUserCrudCallback('', data);
                    return;
                }

                usersModel.getUserByCardId(data, function (error, user) {
                    if (error) {
                        console.error(error);
                        return;
                    }

                    if (user === null) {
                        console.log('Access denied.');
                        console.log('Card id: ' + data + '\n');
                        self.emit('access_denied');
                        return;
                    }

                    console.log('Access granted:');
                    console.log('User id: ' + user.getId());
                    console.log('User name: ' + user.getDisplayName());
                    console.log('User card id: ' + user.getCardId() + '\n');

                    var timestamp = Date.now();
                    var visit = new Visit(null, user, timestamp);
                    visitsModel.addVisit(visit, function (error) {
                        if (error) {
                            console.error(error);
                            return;
                        }

                        self.emit('visit', visit);
                    });
                });
            });
        });
    });
};

util.inherits(FakeCardReader, EventEmitter);

module.exports = new FakeCardReader();
