/**
 * Module: config
 *
 * Reads the config files based on consoleArguments module environment and exports the cofigs data
 */

var fs = require('fs');
var fileNameHelper = require('./helpers/fileNameHelper');
var consoleArguments = require('./consoleArguments');
var configDirectory = './config/' + consoleArguments.environment + '/';
var config = {};

if (!fs.existsSync(configDirectory)) {
    console.error('Error! Invalid environment "%s"!', consoleArguments.environment);
    process.exit();
}

var configFileNames = fs.readdirSync(configDirectory);

configFileNames.forEach(function (configFileName) {
    var fileExtension = fileNameHelper.getFileExtension(configFileName);
    var fileName = fileNameHelper.getFileName(configFileName);

    if (!fileExtension || !fileName) {
        return;
    }

    if (fileExtension !== 'js' && fileExtension !== 'json') {
        return;
    }

    config[fileName] = require(configDirectory + configFileName);
});

module.exports = config;
