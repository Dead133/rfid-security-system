'use strict';

/**
 * Class Admin
 *
 * @param {int}    id          Admin id in the database
 * @param {string} login       Admin login
 * @param {string} password    Admin password
 * @param {string} displayName Admin display name
 *
 * @constructor
 */
var Admin = function (id, login, password, displayName) {
    this.setId(id);
    this.setLogin(login);
    this.setPassword(password);
    this.setDisplayName(displayName);
};

/**
 * Serialize object before saving it as JSON
 *
 * @returns {object}
 */
Admin.prototype.serialize = function () {
    return {
        id: this.getId(),
        login: this.getLogin(),
        password: this.getPassword(),
        displayName: this.getDisplayName()
    };
};

/**
 * Id setter
 *
 * @param {int} id
 */
Admin.prototype.setId = function (id) {
    if (typeof id !== 'number' && id !== null) {
        throw new Error('invalid type. Id should be a number or null.');
    }
    this._id = id;
};

/**
 * Id getter
 *
 * @returns {int}
 */
Admin.prototype.getId = function () {
    return this._id;
};

/**
 * Login setter
 *
 * @param {string} login
 */
Admin.prototype.setLogin = function (login) {
    if (typeof login !== 'string') {
        throw new Error('invalid type. Login should be a string.');
    }
    this._password = login;
};

/**
 * Login getter
 *
 * @returns {string}
 */
Admin.prototype.getLogin = function () {
    return this._password;
};

/**
 * Password setter
 *
 * @param {string} password
 */
Admin.prototype.setPassword = function (password) {
    if (typeof password !== 'string') {
        throw new Error('invalid type. Password should be a string.');
    }
    this._password = password;
};

/**
 * Password getter
 *
 * @returns {string}
 */
Admin.prototype.getPassword = function () {
    return this._password;
};

/**
 * DisplayName setter
 *
 * @param {string} displayName
 */
Admin.prototype.setDisplayName = function (displayName) {
    if (typeof displayName !== 'string') {
        throw new Error('invalid type. DisplayName should be a string.');
    }
    this._displayName = displayName;
};

/**
 * DisplayName getter
 *
 * @returns {string}
 */
Admin.prototype.getDisplayName = function () {
    return this._displayName;
};

module.exports = Admin;
