'use strict';

/**
 * Class EntitiesArray
 *
 * @constructor
 */
var EntitiesArray = function () {

};

EntitiesArray.prototype = [];
EntitiesArray.prototype.constructor = EntitiesArray;

/**
 * Serialize all members of the array and return array of serialized objects
 *
 * @returns {object[]}
 */
EntitiesArray.prototype.serialize = function () {
    var serializedArray = [];

    this.forEach(function(object) {
        if (typeof object.serialize !== 'function') {
            throw new Error('Could not serialize object.');
        }

        serializedArray.push(object.serialize());
    });

    return serializedArray;
};

module.exports = EntitiesArray;
