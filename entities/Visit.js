'use strict';

/**
 * Class Visit
 *
 * @param {int}  id        Visit id in the database
 * @param {User} user      Visitor(user)
 * @param {int}  timestamp Timestamp of the visit moment
 *
 * @constructor
 */
var Visit = function (id, user, timestamp) {
    this.setId(id);
    this.setUser(user);
    this.setTimestamp(timestamp);
};

/**
 * Serialize object before saving it as JSON
 *
 * @returns {object}
 */
Visit.prototype.serialize = function () {
    return {
        id: this.getId(),
        user: this.getUser() === null ? null : this.getUser().serialize(),
        timestamp: this.getTimestamp()
    };
};

/**
 * Id setter
 * @param {int} id
 */
Visit.prototype.setId = function (id) {
    if (typeof id !== 'number' && id !== null) {
        throw new Error('invalid type. Id should be a number or null.');
    }
    this._id = id;
};

/**
 * Id getter
 * @returns {int}
 */
Visit.prototype.getId = function () {
    return this._id;
};

/**
 * User setter
 * @param {User} user
 */
Visit.prototype.setUser = function (user) {
    if (typeof user !== 'object') {
        throw new Error('invalid type. User should be an object or null.');
    }
    this._user = user;
};

/**
 * User getter
 * @returns {User}
 */
Visit.prototype.getUser = function () {
    return this._user;
};

/**
 * Timestamp setter
 * @param {int} timestamp
 */
Visit.prototype.setTimestamp = function (timestamp) {
    if (typeof timestamp !== 'number') {
        throw new Error('invalid type. Timestamp should be a number.');
    }
    this._timestamp = timestamp;
};

/**
 * Timestamp getter
 * @returns {int}
 */
Visit.prototype.getTimestamp = function () {
    return this._timestamp;
};

module.exports = Visit;
