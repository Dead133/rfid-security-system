'use strict';

/**
 * Class User
 *
 * @param {int}    id          User id in the database
 * @param {string} displayName User display name
 * @param {string} cardId      User's RFID card id
 * @param {boolean}   hasPhoto    Does user has a photo
 *
 * @constructor
 */
var User = function (id, displayName, cardId, hasPhoto) {
    this.setId(id);
    this.setDisplayName(displayName);
    this.setCardId(cardId);
    this.setHasPhoto(hasPhoto);
};

/**
 * Serialize object before saving it as JSON
 *
 * @returns {object}
 */
User.prototype.serialize = function () {
    return {
        id: this.getId(),
        displayName: this.getDisplayName(),
        cardId: this.getCardId(),
        hasPhoto: this.getHasPhoto()
    };
};

/**
 * Id setter
 *
 * @param {int} id
 */
User.prototype.setId = function (id) {
    if (typeof id !== 'number' && id !== null) {
        throw new Error('invalid type. Id should be a number or null.');
    }
    this._id = id;
};

/**
 * Id getter
 *
 * @returns {int}
 */
User.prototype.getId = function () {
    return this._id;
};

/**
 * DisplayName setter
 *
 * @param {string} displayName
 */
User.prototype.setDisplayName = function (displayName) {
    if (typeof displayName !== 'string') {
        throw new Error('invalid type. DisplayName should be a string.');
    }
    this._displayName = displayName;
};

/**
 * DisplayName getter
 *
 * @returns {string}
 */
User.prototype.getDisplayName = function () {
    return this._displayName;
};

/**
 * CardId setter
 *
 * @param {string} cardId
 */
User.prototype.setCardId = function (cardId) {
    if (typeof cardId !== 'string') {
        throw new Error('invalid type. CardId should be a string.');
    }
    this._cardId = cardId;
};

/**
 * CardId getter
 *
 * @returns {string}
 */
User.prototype.getCardId = function () {
    return this._cardId;
};

/**
 * HasPhoto setter
 *
 * @param {boolean} hasPhoto
 */
User.prototype.setHasPhoto = function (hasPhoto) {
    if (typeof hasPhoto !== 'boolean') {
        throw new Error('invalid type. HasPhoto should be a boolean.');
    }
    this._hasPhoto = hasPhoto;
};

/**
 * HasPhoto getter
 *
 * @returns {bool}
 */
User.prototype.getHasPhoto = function () {
    return this._hasPhoto;
};

module.exports = User;
