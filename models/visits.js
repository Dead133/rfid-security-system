/**
 * Visits model
 */

'use strict';

var Database = require('../database/Database');
var Visit = require('../entities/Visit');
var User = require('../entities/User');
var EntitiesArray = require('../entities/EntitiesArray');

var visitsModel = {};

/**
 * Add a new visit to database
 *
 * @param {Visit}            visit    Instance of the Visit entity
 * @param {function(string)} callback Callback
 */
visitsModel.addVisit = function (visit, callback) {
    var query = 'INSERT INTO `visits` (`user_id`, `timestamp`) VALUES (?, ?);';

    var userId = visit.getUser().getId();
    var timestamp = Math.floor(visit.getTimestamp() / 1000);

    var parameters = [
        userId,
        timestamp
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        visit.setId(result.insertId);

        callback('');
    });
};

/**
 * Get all visits from database
 *
 * @param {int}                             start    Number of record to start with
 * @param {int}                             limit    Maximum number of records to get from database
 * @param {function(string, EntitiesArray)} callback Callback
 */
visitsModel.getVisits = function (start, limit, callback) {
    var query = 'SELECT `visits`.`id`, `visits`.`user_id`, `visits`.`timestamp`, ' +
                '       `users`.`display_name`, `users`.`card_id`, `users`.`has_photo` ' +
                'FROM `visits` LEFT JOIN `users` ON `visits`.`user_id` = `users`.`id` ' +
                'ORDER BY `visits`.`id` DESC LIMIT ? , ?;';

    var parameters = [
        start,
        limit
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        var visits = new EntitiesArray();

        result.forEach(function (element, index, array) {
            var user;
            if (element['display_name'] !== null && element['card_id'] !== null &&
                element['has_photo'] !== null) {
                var userId = +element['user_id'];
                var userDisplayName = element['display_name'];
                var userCardId = element['card_id'];
                var userHasPhoto = !!element['has_photo'];
                user = new User(userId, userDisplayName, userCardId, userHasPhoto);
            } else {
                user = null;
            }

            var visitId = +element['id'];
            var visitTimestamp = +element['timestamp'];
            var visit = new Visit(visitId, user, visitTimestamp);

            void(index);
            void(array);

            visits.push(visit);
        });

        callback('', visits);
    });
};

/**
 * Get total count of visits in database
 *
 * @param {function(string, int|null)} callback Callback
 */
visitsModel.getVisitsCount = function (callback) {
    var query = 'SELECT COUNT(*) AS `visits_count` FROM `visits`;';

    Database.query(query, [], function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        var visitsCount = result[0]['visits_count'];

        callback('', visitsCount);
    });
};

module.exports = visitsModel;
