/**
 * Users model
 */

'use strict';

var Database = require('../database/Database');
var User = require('../entities/User');
var EntitiesArray = require('../entities/EntitiesArray');
var usersModel = {};

/**
 * Get user by his RFID card id
 *
 * @param {string}                 cardId   User's RFID card id
 * @param {function(string, User)} callback Callback
 */
usersModel.getUserByCardId = function (cardId, callback) {
    var query = 'SELECT * FROM `users` WHERE `card_id` = ?;';

    var parameters = [
        cardId
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        if (result.length !== 1) {
            callback('', null);
            return;
        }

        var userData = result[0];

        var id = +userData['id'];
        var displayName = userData['display_name'];
        var cardId = userData['card_id'];
        var hasPhoto = !!userData['has_photo'];

        var user = new User(id, displayName, cardId, hasPhoto);

        callback('', user);
    });
};

/**
 * Get user by his database record id
 *
 * @param {int}                      userId   If of the user in the database
 * @param {function(string, object)} callback Callback
 */
usersModel.getUserById = function (userId, callback) {
    var query = 'SELECT * FROM `users` WHERE `id` = ?;';

    var parameters = [
        userId
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        if (result.length !== 1) {
            callback('', null);
            return;
        }

        var userData = result[0];

        var id = +userData['id'];
        var displayName = userData['display_name'];
        var cardId = userData['card_id'];
        var hasPhoto = !!userData['has_photo'];

        var user = new User(id, displayName, cardId, hasPhoto);

        callback('', user);
    });
};

/**
 * Get users from the database
 *
 * @param {int}                             start    Number of record to start with
 * @param {int}                             limit    Maximum number of records to get from database
 * @param {function(string, EntitiesArray)} callback Callback
 */
usersModel.getUsers = function (start, limit, callback) {
    var query = 'SELECT * FROM `users` LIMIT ?, ?;';

    var parameters = [
        start,
        limit
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        var users = new EntitiesArray();

        result.forEach(function (element, index, array) {
            var id = +element['id'];
            var displayName = element['display_name'];
            var cardId = element['card_id'];
            var hasPhoto = !!element['has_photo'];
            var user = new User(id, displayName, cardId, hasPhoto);

            void(index);
            void(array);

            users.push(user);
        });

        callback('', users);
    });
};

/**
 * Get the total count of users in the database
 *
 * @param {function(string, int|null)} callback Callback
 */
usersModel.getUsersCount = function (callback) {
    var query = 'SELECT COUNT(*) AS `users_count` FROM `users`;';

    Database.query(query, [], function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        var usersCount = result[0]['users_count'];

        callback('', usersCount);
    });
};

/**
 * Add a new user to the database
 *
 * @param {User}             user     Instance of the User entity
 * @param {function(string)} callback Callback
 */
usersModel.addUser = function (user, callback) {
    var displayName = user.getDisplayName();
    var cardId = user.getCardId();
    var hasPhoto = user.getHasPhoto() ? 1 : 0;

    var query = 'INSERT INTO `users` (`display_name`, `card_id`, `has_photo`) VALUES (?, ?, ?);';

    var parameters = [
        displayName,
        cardId,
        hasPhoto
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        user.setId(result.insertId);

        callback('');
    });
};

/**
 * Delete the user record from the database
 *
 * @param {int}              userId   Id of the user in database
 * @param {function(string)} callback Callback
 */
usersModel.deleteUser = function (userId, callback) {
    var query = 'DELETE FROM `users` WHERE `id` = ?;';

    var parameters = [
        userId
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

/**
 * Update user's data in the database
 *
 * @param {User}             user     Instance of the User entity
 * @param {function(string)} callback Callback
 */
usersModel.updateUser = function (user, callback) {

    var id = user.getId();
    var displayName = user.getDisplayName();
    var cardId = user.getCardId();
    var hasPhoto = user.getHasPhoto() ? 1 : 0;

    var query = 'UPDATE `users` SET `display_name` = ?, `card_id` = ?, `has_photo` = ? ' +
                'WHERE `id` = ?;';

    var parameters = [
        displayName,
        cardId,
        hasPhoto,
        id
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        user.setId(result.insertId);

        callback('');
    });
};

module.exports = usersModel;
