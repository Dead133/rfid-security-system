/**
 * Admins model
 */

'use strict';

var Database = require('../database/Database');
var Admin = require('../entities/Admin');
var adminsModel = {};

/**
 * Get admin by database record id
 *
 * @param {int}                          id       Admin's database record id
 * @param {function(string, Admin|null)} callback Callback
 */
adminsModel.getAdminById = function (id, callback) {
    var query = 'SELECT * FROM `admins` WHERE `id` = ?;';

    var parameters = [
        +id
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        if (result.length !== 1) {
            callback('', null);
            return;
        }

        var adminData = result[0];

        var id = +adminData['id'];
        var login = adminData['login'];
        var password = adminData['password'];
        var displayName = adminData['display_name'];

        var admin = new Admin(id, login, password, displayName);

        callback('', admin);
    });
};

/**
 * Get admin by his login
 *
 * @param {string}                       login    Admin's login
 * @param {function(string, Admin|null)} callback Callback
 */
adminsModel.getAdminByLogin = function (login, callback) {
    var query = 'SELECT * FROM `admins` WHERE `login` = ?;';

    var parameters = [
        login
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error, null);
            return;
        }

        if (result.length !== 1) {
            callback('', null);
            return;
        }

        var adminData = result[0];

        var id = +adminData['id'];
        var login = adminData['login'];
        var password = adminData['password'];
        var displayName = adminData['display_name'];

        var admin = new Admin(id, login, password, displayName);

        callback('', admin);
    });
};

/**
 * Updates admin password in database
 *
 * @param {int}              id           Admin's database record id
 * @param {string}           passwordHash Bcrypt hash of the password
 * @param {function(string)} callback     Callback
 */
adminsModel.updateAdminPassword = function (id, passwordHash, callback) {
    var query = 'UPDATE `admins` SET `password` = ? WHERE `id` = ?;';

    var parameters = [
        passwordHash,
        +id
    ];

    Database.query(query, parameters, function (error, result) {
        if (error) {
            callback(error);
            return;
        }

        void(result);

        callback('');
    });
};

module.exports = adminsModel;
