'use strict';

/**
 * Module: fileNameHelper
 *
 * Set of some simple functions to work with the file names
 */

var fileNameHelper = {};

/**
 * Returns file extension
 *
 * @param {string} fullFileName - file name (without path)
 *
 * @returns {string} file extension
 */
fileNameHelper.getFileExtension = function (fullFileName) {
    return fullFileName.split('.').pop();
};

/**
 * Returns file name without extension
 *
 * @param {string} fullFileName - file name (without path)
 *
 * @returns {string} file name without extension
 */
fileNameHelper.getFileName = function (fullFileName) {
    if (fullFileName.indexOf('.') === -1) {
        return null
    }

    return fullFileName.substr(0, fullFileName.lastIndexOf('.'));
};

module.exports = fileNameHelper;
