var webserverConfig = {};

webserverConfig.port = 80;
webserverConfig.secretKey = 'rg6AB2tLGfm75gHF9rBQ1PoW0SgDtMKgh78';
webserverConfig.sessionMaxAge = 12 * 3600 * 1000; // Twelve hours long session
webserverConfig.cookieName = 'rfid.sid';

module.exports = webserverConfig;
