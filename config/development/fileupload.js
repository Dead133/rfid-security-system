var fileuploadConfig = {};

fileuploadConfig.maxFileSize = 10 * 1024 * 1024; // 10 Mb
fileuploadConfig.allowedMimeTypes = [
    'image/jpg',
    'image/jpeg'
];

module.exports = fileuploadConfig;
