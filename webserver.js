console.log('Loading webserver...');

var config = require('./config');
var fs = require('fs');
//var multer = require('multer');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var express = require('express');
var session = require('express-session');
var MySQLStore = require('connect-mysql')({session: session});
var expressHandlebars = require('express-handlebars');
var databaseConnectionPool = require('./database/databaseConnectionPool');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var cardReader = config['hardware'].hardwareEnabled ? require('./cardReader') :
                 require('./fakeCardReader');

// View engine
app.engine('.hbs', expressHandlebars({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: __dirname + '/templates/layouts',
    partialsDir: __dirname + '/templates/partials'
}));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/templates');

// Serve static files
app.use(express.static('static'));

// Urlencoded parser for post requests
app.use(bodyParser.urlencoded({
    extended: true
}));

//app.use(multer({dest: '/tmp'}));

// Not sure bodyParser.json() is required here
//app.use(bodyParser.json());

//sessions
app.use(cookieParser(config['webserver'].secretKey));
app.use(session({
    store: new MySQLStore({
        pool: databaseConnectionPool
    }),
    secret: config['webserver'].secretKey,
    name: config['webserver'].cookieName,
    cookie: {
        maxAge: config['webserver'].sessionMaxAge
    },
    resave: true,
    saveUninitialized: true
}));

// Dynamic loading of controllers
console.log('Including controllers...');
fs.readdirSync('./controllers').forEach(function (file) {
    if (file.substr(-3) === '.js') {
        var controllerName = file.substring(0, file.length - 3);
        console.log('Including controller ' + controllerName);
        var controller = require('./controllers/' + file);
        controller(app);
    }
});
console.log('Done.');

// 404 handler
app.use(function (req, res, next) {
    void(next);
    res.status(404);
    res.render('404', {
        header: '404. Page not found'
    });
});

// 500 handler
app.use(function (err, req, res, next) {
    void(next);
    console.error(err.stack);
    res.status(500);
    res.render('500', {
        header: '500. Internal server error',
        error: err.stack
    });
});

http.listen(config['webserver'].port, function () {
    console.log('Webserver ready.');
    cardReader.turnOnReadyLed();
});

io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});

cardReader.on('visit', function (visit) {
    "use strict";
    io.sockets.emit('visit', visit.serialize());
});
